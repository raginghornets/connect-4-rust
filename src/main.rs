use std::{fmt::{self, Display}, io::Write, num::ParseIntError};

const NUM_ROWS: usize = 6;
const NUM_COLS: usize = 7;

#[derive(Clone, PartialEq)]
enum State {
    Empty,
    Yellow,
    Red,
}

impl State {
    fn alternate(&self) -> State {
        match self {
            State::Yellow => State::Red,
            State::Red => State::Yellow,
            State::Empty => State::Empty,
        }
    }
}

impl Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            State::Empty => " ",
            State::Yellow => "Y",
            State::Red => "R",
        })
    }
}

struct Board {
    grid: Vec<Vec<State>>,
}

impl Board {
    fn new() -> Board {
        Board {
            grid: vec![vec![State::Empty; NUM_COLS]; NUM_ROWS],
        }
    }

    fn insert(&mut self, column: usize, color: State) {
        let mut row = 0;
        if column >= NUM_COLS {
            println!("Invalid column.\n");
        }

        while row + 1 < NUM_ROWS && *self.grid.get(row + 1).unwrap().get(column).unwrap() == State::Empty {
            row += 1;
        }

        if row == 0 && self.grid.get_mut(row).unwrap()[column] != State::Empty {
            println!("Column is full! Row: {}", row);
            return;
        }

        self.grid.get_mut(row).unwrap()[column] = color;
    }

    fn is_full(&self) -> bool {
        for s in self.grid.get(0).unwrap() {
            if *s == State::Empty {
                return false;
            }
        }
        true
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        let mut string = String::new();
        for row in &self.grid {
            for col in row {
                string.push_str(format!("| {} ", col).as_str());
            }
            string.push_str("|\n");
        }
        write!(f, "{}", string)
    }
}

fn input() -> Result<usize, ParseIntError> {
    let mut s = String::new();
    
    print!("Column: ");

    std::io::stdout().flush().unwrap();

    std::io::stdin()
        .read_line(&mut s)
        .expect("Failed to read line");

    let s = s.trim().parse::<usize>();

    match s {
        Ok(n) if n < NUM_COLS => Ok(n),
        _ => {
            println!("Invalid input.");
            input()
        }
    }
}

fn main() {
    let mut board = Board::new();

    let mut current_color = State::Red;
    
    while !board.is_full() {
        println!("{}", board);
        let col = input().unwrap();
        let color = current_color.alternate();
        board.insert(col, color);
        current_color = current_color.alternate();
    }
    println!("Game over!");
    println!("{}", board);
}
